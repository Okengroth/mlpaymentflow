//
//  CardIssuerCollectionViewCell.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/23/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit
import SDWebImage

class CardIssuerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardIssuerThumbnail: UIImageView!
    @IBOutlet weak var cardIssuerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(withIssuer cardIssuer:CardIssuer) {
        setupUI()
        if let name = cardIssuer.name {
            cardIssuerName.text = name
        }
        
        if let secureThumbnailUri = cardIssuer.secureThumbnail {
            cardIssuerThumbnail.sd_setImage(with: URL(string: secureThumbnailUri), placeholderImage:nil)
        }
    }

    func setupUI() {
        contentView.layer.cornerRadius = 2.0
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true;
        
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width:0,height: 2.0)
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.contentView.layer.cornerRadius).cgPath

    }
}
