//
//  PaymentMethodTableViewCell.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/22/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit
import SDWebImage

class PaymentMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var paymentMethodThumbnail: UIImageView!
    @IBOutlet weak var paymentMethodName: UILabel!
    
    @IBOutlet weak var paymentMethodMinimumValue: UILabel!
    @IBOutlet weak var paymentMethodMaximumValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(withPaymentMethod method:PaymentMethod) {
        if let name = method.name {
            paymentMethodName.text = name
        }
        if let secureThumbnailUri = method.secureThumbnail {
            paymentMethodThumbnail.sd_setImage(with: URL(string: secureThumbnailUri), placeholderImage:nil)
        }
        if let minimumValue = method.minAllowedAmount {
            paymentMethodMinimumValue.text = String(minimumValue)
        }
        
        if let maximumValue = method.maxAllowedAmount {
            paymentMethodMaximumValue.text = String(maximumValue)
        }
    }

}
