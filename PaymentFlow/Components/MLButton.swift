//
//  MLButton.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/21/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit

class MLButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    func initialize() {
        // Button gral. UI
        backgroundColor = Constants.Color.Blue
        layer.cornerRadius = 3.0
        
        // Button Title
        titleLabel?.font = UIFont(name: Constants.Font.HelveticaBold, size: 17.0)
        setTitleColor(.white, for: .normal)
    }
}
