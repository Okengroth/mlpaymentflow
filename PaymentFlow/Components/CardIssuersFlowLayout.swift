//
//  CardIssuersFlowLayout.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/22/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit

class CardIssuersFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    override var itemSize: CGSize {
        set {
            
        }
        get {
            let numberOfColumns: CGFloat = 2
            
            let itemWidth = (self.collectionView!.frame.width - (numberOfColumns + 5)) / numberOfColumns
            return CGSize(width: itemWidth, height: 100)
        }
    }
    
    func setupLayout() {
        minimumInteritemSpacing = 5
        minimumLineSpacing = 5
        scrollDirection = .vertical
    }
}
