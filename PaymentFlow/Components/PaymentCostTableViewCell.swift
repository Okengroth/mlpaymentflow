//
//  PaymentCostTableViewCell.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/24/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit

class PaymentCostTableViewCell: UITableViewCell {

    @IBOutlet weak var installmentDescription: UILabel!
    @IBOutlet weak var totalAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCell(with payerCost:PayerCost) {
        installmentDescription.text = payerCost.recommendedMessage
        if let amount = payerCost.totalAmount {
            totalAmount.text = String(amount)
        }
    }
}
