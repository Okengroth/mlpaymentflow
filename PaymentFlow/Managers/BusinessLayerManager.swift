//
//  BusinessLayerManager.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/22/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit
import ObjectMapper
class BusinessLayerManager: NSObject {

    static let sharedInstance = BusinessLayerManager()
    
    private var apiKey:String!
    
    private override init() {
        super.init()
        initialize()
    }
    
    private func initialize() {
        
    }
    
    func getPaymentMethod(success:@escaping ([PaymentMethod]) -> Void, failure:@escaping (Error) -> Void) {

        NetworkLayerManager.sharedInstance.getRequest(baseUrl: Constants.Endpoint.Base,
                                                      uri: Constants.Endpoint.PaymentMethod,
                                                      paramDict: NetworkLayerHelper.getPaymentMethodRequestParameters() ,
        success: { (JSONResponse) -> Void in
            if let paymentMethods = Mapper<PaymentMethod>().mapArray(JSONString: JSONResponse) {
                success(paymentMethods)
            }
        }) {
            (error) -> Void in
            failure(error)
        }
    }
    
    
    func getCardIssuers(withPaymentMethod paymentMethodId: String, success:@escaping ([CardIssuer]) -> Void, failure:@escaping (Error) -> Void) {
        
        NetworkLayerManager.sharedInstance.getRequest(baseUrl: Constants.Endpoint.Base,
                                                      uri: Constants.Endpoint.CardIssuer,
                                                      paramDict: NetworkLayerHelper.getAvailableIssuersRequestParameters(withPayment: paymentMethodId),
            success: { (JSONResponse) -> Void in
                if let cardIssuers = Mapper<CardIssuer>().mapArray(JSONString: JSONResponse) {
                    success(cardIssuers)
                }
        }) {
            (error) -> Void in
            failure(error)
        }
    }
    
    func getInstallments(amount: Int, paymentMethodId: String, issuerId: String,
                         success:@escaping (PaymentDetail) -> Void, failure:@escaping (Error) -> Void) {
        
        NetworkLayerManager.sharedInstance.getRequest(baseUrl: Constants.Endpoint.Base,
                                                      uri: Constants.Endpoint.QuotasNumber,
                                                      paramDict: NetworkLayerHelper.getInstallmentsRequestParameters(withAmount: amount, paymentId: paymentMethodId, issuerId: issuerId),
                                                      success: {
                                                        (JSONResponse) -> Void in
                                                        if let paymentDetailArray = Mapper<PaymentDetail>().mapArray(JSONString: JSONResponse),
                                                            let paymentDetail = paymentDetailArray.first {
                                                            success(paymentDetail)
                                                        }
        }) {
            (error) -> Void in
            failure(error)
        }
    }
}
