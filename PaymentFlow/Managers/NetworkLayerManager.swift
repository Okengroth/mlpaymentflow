//
//  NetworkLayerManager.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/22/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
class NetworkLayerManager: NSObject {
    static let sharedInstance = NetworkLayerManager()
    
    private override init() {}

    func getRequest(baseUrl:String, uri: String, paramDict:Dictionary<String, String>? = nil,
                    success:@escaping (String) -> Void, failure:@escaping (Error) -> Void) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let urlString = baseUrl.appending(uri)
        Alamofire.request(URL(string: urlString)!,
                          method: .get,
                          parameters: paramDict)
            .responseJSON { (response:DataResponse<Any>) in
                debugPrint(response)
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                switch(response.result) {
                case .success(_):
                    if let data = response.data,
                        let json : String = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as? String {
                        success(json)
                    }
                    break
                    
                case .failure(_):
                    if let error = response.result.error {
                        let JSONError = error as NSError
                        failure(JSONError)
                    }
                    break
                    
                }
        }
    }
}
