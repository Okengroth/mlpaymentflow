//
//  PaymentFlowManager.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/24/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit

class PaymentFlowManager: NSObject {
    
    static let sharedInstance = PaymentFlowManager()
    
    private var paymentSummary = PaymentSummary()
    
    private override init() {
        super.init()
    }
    
    func newPaymentFlow() {
        paymentSummary.clearValues()
    }
    
    func setPaymentAmount(withValue paymentAmount: Int) {
        paymentSummary.paymentAmount = paymentAmount
    }
    
    func setPaymentFinalAmount(withValue paymentAmount: Int) {
        paymentSummary.paymentfinalAmount = paymentAmount
    }
    
    func setPaymentMethod(withId paymentMethodId: String) {
        paymentSummary.paymentMethodId = paymentMethodId
    }
    
    func setIssuer(withId issuerId: String) {
        paymentSummary.issuerId = issuerId
    }
    
    func setInstallment(withInstallment value: Int, andMessage recommendedMessage:String) {
        paymentSummary.installmentNumber = value
        paymentSummary.installmentDescription = recommendedMessage
    }
    
    func getPaymentFlowSummary() -> PaymentSummary {
        return paymentSummary
    }
}
