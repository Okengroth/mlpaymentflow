//
//  NetworkLayerHelper.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/22/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit

class NetworkLayerHelper: NSObject {
    
    static var apiKey: String {
        return "444a9ef5-8a6b-429f-abdf-587639155d88"
    }
    
    static func getPaymentMethodRequestParameters() -> Dictionary<String, String> {
        let parameters = [Constants.Parameters.PublicKey : apiKey]
        return parameters
    }
    
    static func getAvailableIssuersRequestParameters(withPayment paymentId:String)
            ->Dictionary<String, String> {
        let parameters = [Constants.Parameters.PublicKey : apiKey,
                          Constants.Parameters.PaymentMethodId: paymentId]
        return parameters
    }
    
    static func getInstallmentsRequestParameters(withAmount amount: Int, paymentId: String, issuerId: String)
        ->Dictionary<String, String> {
            let parameters = [Constants.Parameters.PublicKey : apiKey,
                              Constants.Parameters.PaymentMethodId : paymentId,
                              Constants.Parameters.IssuerId : issuerId,
                              Constants.Parameters.Amount : String(amount)]
            return parameters
    }
}
