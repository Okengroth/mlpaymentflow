//
//  PaymentMethod.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/22/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit
import ObjectMapper

class PaymentMethod: Mappable {
    
    var id: String?
    var name: String?
    var paymentTypeId: String?
    var secureThumbnail: String?
    var thumbnail: String?
    var deferredCapture: String?
    var minAllowedAmount: Int?
    var maxAllowedAmount: Int?
    var accreditationTime: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        name                <- map["name"]
        paymentTypeId       <- map["payment_type_id"]
        secureThumbnail     <- map["secure_thumbnail"]
        thumbnail           <- map["thumbnail"]
        deferredCapture     <- map["deferred_capture"]
        minAllowedAmount    <- map["min_allowed_amount"]
        maxAllowedAmount    <- map["max_allowed_amount"]
        accreditationTime   <- map["accreditation_time"]
    }
}
