//
//  PayerCost.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/23/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit
import ObjectMapper

class PayerCost: Mappable {
    var installments: Int?
    var installmentRate: Int?
    var discountRate: Int?
    var minAllowedAmount: Int?
    var maxAllowedAmount: Int?
    var recommendedMessage: String?
    var installmentAmount: Int?
    var totalAmount: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        installments        <- map["installments"]
        installmentRate     <- map["installment_rate"]
        discountRate        <- map["discount_rate"]
        minAllowedAmount    <- map["min_allowed_amount"]
        maxAllowedAmount    <- map["max_allowed_amount"]
        recommendedMessage  <- map["recommended_message"]
        installmentAmount   <- map["installment_amount"]
        totalAmount         <- map["total_amount"]
    }
}
