//
//  PaymentDetail.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/23/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit
import ObjectMapper

class PaymentDetail: Mappable {
    var paymentMethodId: String?
    var paymentTypeId: String?
    var issuer: CardIssuer?
    var payerCosts: [PayerCost]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        paymentMethodId             <- map["payment_method_id"]
        paymentTypeId               <- map["payment_type_id"]
        issuer                      <- map["issuer"]
        payerCosts                  <- map["payer_costs"]
    }
}
