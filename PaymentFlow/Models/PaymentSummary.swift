//
//  PaymentSummary.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/24/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit

class PaymentSummary: NSObject {
    
    var paymentAmount: Int?
    var paymentMethodId: String?
    var issuerId: String?
    var installmentNumber: Int?
    var installmentDescription: String?
    var paymentfinalAmount: Int?
    
    func PaymentSummary() {
        clearValues()
    }
    
    func clearValues() {
        self.paymentAmount = 0
        self.paymentfinalAmount = 0
        self.installmentNumber = 0
        self.paymentMethodId = ""
        self.installmentDescription = ""
        self.issuerId = ""
    }
    
}
