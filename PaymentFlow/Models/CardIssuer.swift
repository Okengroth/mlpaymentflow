//
//  CardIssuer.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/22/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit
import ObjectMapper

class CardIssuer: Mappable {
    var id: String?
    var name: String?
    var secureThumbnail: String?
    var thumbnail: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        name                <- map["name"]
        secureThumbnail     <- map["secure_thumbnail"]
        thumbnail           <- map["thumbnail"]
    }
}
