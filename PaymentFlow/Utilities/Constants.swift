//
//  Constants.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/21/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit

struct Constants {
    
    // UI Constants
    struct Color {
        static let Yellow = UIColor(red: 255.0/255.0, green: 242.0/255.0, blue: 42.0/255.0, alpha: 1.0)
        static let Gray = UIColor(red: 34.0/255.0, green: 34.0/255.0, blue: 34.0/255.0, alpha: 1.0)
        static let Blue = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 98.0/255.0, alpha: 1.0)
    }
    
    struct Font {
        static let Helvetica = "HelveticaNeue"
        static let HelveticaBold = "HelveticaNeue-Bold"
    }
}

// Networking Constants
extension Constants {
    
    struct Endpoint {
        static let Base = "https://api.mercadopago.com/v1/"
        static let PaymentMethod = "payment_methods"
        static let CardIssuer = "payment_methods/card_issuers"
        static let QuotasNumber = "payment_methods/installments"
    }
    
    struct Parameters {
        static let PublicKey = "public_key"
        static let PaymentMethodId = "payment_method_id"
        static let Amount = "amount"
        static let IssuerId = "issuer.id"
    }
}

// Storyboards
extension Constants {
    
    struct Segue {
        static let FirstStepToSecondStepSegue = "segueToSecondStep"
        static let SecondStepToThirdStepSegue = "segueToThirdStep"
        static let ThirdStepToForthStepSegue = "segueToFourthStep"
        static let UnwindToFirstStep = "unwindToFirstStep"
        static let SegueToSummaryContainer = "segueToSummaryContainer"
    }
    
    struct CellIdentifier {
        static let PaymentMethodCell = "PaymentMethodCell"
        static let CardIssuerCell = "CardIssuerCell"
        static let InstallmentCell = "PayerCostCell"
    }
    
    struct Nib {
        static let PaymentMethod = "PaymentMethodCell"
        static let CardIssuer = "CardIssuerCell"
        static let Installment = "PaymentCostCell"
    }
    
}
