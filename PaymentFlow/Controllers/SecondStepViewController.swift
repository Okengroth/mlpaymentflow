//
//  SecondStepViewController.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/22/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit
import PKHUD

class SecondStepViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var paymentMethods: [PaymentMethod] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: Constants.Nib.PaymentMethod, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.PaymentMethodCell)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "Seleccionar Tarjeta"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK:- Navigation 
extension SecondStepViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segue.SecondStepToThirdStepSegue {
            let thirdStepVC = segue.destination as! ThirdStepViewController
            thirdStepVC.cardIssuers = sender as! [CardIssuer]
        }
    }
}

// MARK:- Get Data
extension SecondStepViewController{
    func getPaymentMethod(withId paymentMethodId: String) {
        PKHUD.sharedHUD.show()
        BusinessLayerManager.sharedInstance.getCardIssuers(withPaymentMethod: paymentMethodId,
                                                            success: { (cardIssuers) -> Void in
            PKHUD.sharedHUD.hide(true)
            self.performSegue(withIdentifier: Constants.Segue.SecondStepToThirdStepSegue, sender: cardIssuers)
        }, failure: { (error) -> Void in
            PKHUD.sharedHUD.hide(true)
            let errorAlert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
            errorAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(errorAlert, animated: true, completion: nil)

        })
    }
}

// MARK:- TableView Delegate
extension SecondStepViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedPaymentMethod = paymentMethods[indexPath.row]
        if let minAmount = selectedPaymentMethod.minAllowedAmount,
            let maxAmount = selectedPaymentMethod.maxAllowedAmount,
            let paymentAmount = PaymentFlowManager.sharedInstance.getPaymentFlowSummary().paymentAmount {
            if Int(minAmount) > paymentAmount || Int(maxAmount) < paymentAmount {
                let errorAlert = UIAlertController(title: "No es posible usar esta tarjeta", message: "El monto a pagar no corresponde con en el rango de la tarjeta", preferredStyle: UIAlertControllerStyle.alert)
                errorAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(errorAlert, animated: true, completion: nil)
                return
            }
        }
        
        if let paymentMethodId = selectedPaymentMethod.id {
            PaymentFlowManager.sharedInstance.setPaymentMethod(withId: paymentMethodId)
            getPaymentMethod(withId: paymentMethodId)
        }
    }
}

// MARK:- TableView DataSource
extension SecondStepViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentMethods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.PaymentMethodCell) as! PaymentMethodTableViewCell
        cell.setupCell(withPaymentMethod: paymentMethods[indexPath.row])
        return cell
    }
}
