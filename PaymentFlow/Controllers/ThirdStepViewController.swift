//
//  ThirdStepViewController.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/22/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit
import PKHUD

class ThirdStepViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var emptyState: UIView!
    @IBOutlet weak var noResultsIcon: UIImageView!
    
    var cardIssuers: [CardIssuer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if cardIssuers.count > 0 {
            navigationItem.title = "Seleccionar Banco Emisor"
            emptyState.isHidden = true
        } else {
            noResultsIcon.image = noResultsIcon.image?.withRenderingMode(.alwaysTemplate)
            noResultsIcon.tintColor = Constants.Color.Blue
            navigationItem.title = "Sin resultados"
            emptyState.isHidden = false
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func popToSecondStep(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
}


// MARK:- Navigation
extension ThirdStepViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segue.ThirdStepToForthStepSegue {
            let forthStepVC = segue.destination as! ForthStepViewController
            forthStepVC.payerCosts = sender as! [PayerCost]
        }
    }
}


// MARK:- Get Data
extension ThirdStepViewController {
    func getInstallments(forCardIssuer issuerId: String) {
        PKHUD.sharedHUD.show()
        let paymentSummary = PaymentFlowManager.sharedInstance.getPaymentFlowSummary()
        BusinessLayerManager.sharedInstance.getInstallments(amount: paymentSummary.paymentAmount!, paymentMethodId: paymentSummary.paymentMethodId!, issuerId: paymentSummary.issuerId!, success: { (paymentDetail) -> Void in
            PKHUD.sharedHUD.hide(true)
            self.performSegue(withIdentifier: Constants.Segue.ThirdStepToForthStepSegue, sender: paymentDetail.payerCosts)
        }, failure: { (error) -> Void in
            PKHUD.sharedHUD.hide(true)
            let errorAlert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
            errorAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(errorAlert, animated: true, completion: nil)
        })
    }
}

// MARK:- CollectionView Methods
extension ThirdStepViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func setupCollectionView() {
        let collectionViewLayout = CardIssuersFlowLayout()
        collectionView.contentInset = UIEdgeInsetsMake(5, 0, 5, 0)  
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: Constants.Nib.CardIssuer, bundle: nil), forCellWithReuseIdentifier: Constants.CellIdentifier.CardIssuerCell)
        collectionView.collectionViewLayout = collectionViewLayout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardIssuers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.CellIdentifier.CardIssuerCell, for: indexPath) as! CardIssuerCollectionViewCell
        cell.setupCell(withIssuer: cardIssuers[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCardIssuer = cardIssuers[indexPath.row]
        if let cardIssuerId = selectedCardIssuer.id {
            PaymentFlowManager.sharedInstance.setIssuer(withId: cardIssuerId)
            getInstallments(forCardIssuer: cardIssuerId)
        }
    }
}
