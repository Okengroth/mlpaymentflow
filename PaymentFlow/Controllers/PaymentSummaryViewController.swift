//
//  PaymentSummaryViewController.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/24/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit

protocol PaymentSummaryProtocol : AnyObject {
    func dissmissModal()
}

class PaymentSummaryViewController: UIViewController {
    
    @IBOutlet weak var paymentAmount: UILabel!
    @IBOutlet weak var paymentMethod: UILabel!
    @IBOutlet weak var paymentInstallment: UILabel!
    @IBOutlet weak var finalAmount: UILabel!
    @IBOutlet weak var cardIssuer: UILabel!
    
    weak var delegate: PaymentSummaryProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupFields(withSummary paymentSumamry:PaymentSummary) {
        if let amount = paymentSumamry.paymentAmount {
            paymentAmount.text = String(amount)
        }
        if let paymentMethodId = paymentSumamry.paymentMethodId {
            paymentMethod.text = paymentMethodId
        }
        if let issuer = paymentSumamry.issuerId {
            cardIssuer.text = issuer
        }
        if let installmentsDescription = paymentSumamry.installmentDescription {
            paymentInstallment.text = installmentsDescription
        }
        if let amountWithInstallments = paymentSumamry.paymentfinalAmount {
            finalAmount.text = String(amountWithInstallments)
        }
    }
    
    @IBAction func acceptButtonPressed(_ sender: Any) {
        delegate?.dissmissModal()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
