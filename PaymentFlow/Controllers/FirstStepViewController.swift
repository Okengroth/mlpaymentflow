//
//  FirstStepViewController.swift
//  PaymentFlow
//
//  Created by Ariel Demarco on 11/21/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit
import PKHUD

class FirstStepViewController: UIViewController {
    
    @IBOutlet weak var paymentAmount: UITextField!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var summaryModal: UIView!
    
    var paymentSummaryController: PaymentSummaryViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentAmount.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "Monto"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func shouldShowSummaryModel(shouldShow: Bool) {
        summaryModal.isHidden = !shouldShow
        overlayView.isHidden = !shouldShow
        UIView.animate(withDuration: 1, animations: {
            self.summaryModal.alpha = shouldShow ? 1 : 0
        })
    }
}

extension FirstStepViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segue.FirstStepToSecondStepSegue {
            let destionationController = segue.destination as! SecondStepViewController
            destionationController.paymentMethods = sender as! [PaymentMethod]
        } else if (segue.identifier == Constants.Segue.SegueToSummaryContainer) {
            paymentSummaryController = segue.destination as! PaymentSummaryViewController
            paymentSummaryController.delegate = self
        }

        
    }
    
    @IBAction func finishedPaymentFlow(segue:UIStoryboardSegue) {
        paymentAmount.text = ""
        paymentSummaryController.setupFields(withSummary: PaymentFlowManager.sharedInstance.getPaymentFlowSummary())
        shouldShowSummaryModel(shouldShow: true)
    }
    
}

// MARK:- Actions
extension FirstStepViewController {
    
    @IBAction func nextButtonPressed(_ sender: MLButton) {
        if (paymentAmount.text?.characters.count)! > 0 {
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show()
            BusinessLayerManager.sharedInstance.getPaymentMethod(success: {
                (paymentMethods) -> Void in
                PKHUD.sharedHUD.hide(true)
                if let amountString = self.paymentAmount.text,
                    let amount = Int(amountString)  {
                    PaymentFlowManager.sharedInstance.setPaymentAmount(withValue: amount)
                }
                self.performSegue(withIdentifier: Constants.Segue.FirstStepToSecondStepSegue, sender: paymentMethods)
            }, failure: { (error) -> Void in
                PKHUD.sharedHUD.hide(true)
                let errorAlert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                errorAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(errorAlert, animated: true, completion: nil)
            })
        }
    }
}

extension FirstStepViewController : PaymentSummaryProtocol {
    func dissmissModal() {
        shouldShowSummaryModel(shouldShow: false)
    }
}

