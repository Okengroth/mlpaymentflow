//
//  MLNavigationController.swift
//  PaymentFlow
//
//  Created by Ariel Demarco on 11/21/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit

class MLNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.barTintColor = Constants.Color.Yellow
        navigationBar.tintColor = .black
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
