//
//  ForthStepViewController.swift
//  PaymentFlow
//
//  Created by OLX - Ariel Demarco on 11/24/16.
//  Copyright © 2016 Ariel Demarco. All rights reserved.
//

import UIKit

class ForthStepViewController: UIViewController {
    
    var payerCosts: [PayerCost] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: Constants.Nib.Installment, bundle: nil), forCellReuseIdentifier: Constants.CellIdentifier.InstallmentCell)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "Seleccionar Cantidad de Cuotas"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK:- Navigation
extension ForthStepViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}

// MARK:- TableView Delegate
extension ForthStepViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedPayerCost = payerCosts[indexPath.row]
        if let recommendedMessage = selectedPayerCost.recommendedMessage,
            let totalAmount = selectedPayerCost.totalAmount,
            let installmentQuantity = selectedPayerCost.installments {
            PaymentFlowManager.sharedInstance.setInstallment(withInstallment: installmentQuantity, andMessage: recommendedMessage)
            PaymentFlowManager.sharedInstance.setPaymentFinalAmount(withValue: totalAmount)
            performSegue(withIdentifier: Constants.Segue.UnwindToFirstStep, sender: nil)
        }
    }
}

// MARK:- TableView DataSource
extension ForthStepViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return payerCosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.InstallmentCell) as! PaymentCostTableViewCell
        cell.setupCell(with: payerCosts[indexPath.row]) 
        return cell
    }
}
